import sbt._
import sbt.Keys._
import sbtrelease.ReleasePlugin._

object GatlingjwtBuild extends Build {

  lazy val gatlingjwt = Project(
    id = "gatling-jwt",
    base = file("."),
    settings = Project.defaultSettings
        ++ releaseSettings    
        ++ Seq(
            resolvers += "Gatling repo" at "http://repository.excilys.com/content/groups/public",
            resolvers += "Atlassian's Maven Public Repository" at "https://maven.atlassian.com/content/groups/public",
            name := "gatling-jwt",
            organization := "com.atlassian.gatling",
            scalaVersion := "2.10.3",
            publishMavenStyle := true,
            publishArtifact in Test := false,
            libraryDependencies += "com.atlassian.jwt" % "jwt-core" % "1.0-m11",
            libraryDependencies += "com.atlassian.jwt" % "jwt-api" % "1.0-m11",
            libraryDependencies += "io.gatling.highcharts" % "gatling-charts-highcharts" % "2.0.0-M3a",
            libraryDependencies += "io.gatling" % "gatling-app" % "2.0.0-M3a"))

}
