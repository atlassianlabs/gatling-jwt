# gatling-jwt

An extension to Gatling 2 to help send [JWT](http://tools.ietf.org/html/draft-ietf-oauth-json-web-token%E2%80%8E)-signed requests. Early stages, currently only supports GET requests.

## Usage

### Create a Gatling Project

For example, use the [Gatling Maven archetype](https://github.com/excilys/gatling/wiki/Ide-integration).
Note that gatling-jwt is built against Gatling 2.0M3a. 

### Add gatling-jwt as a dependency

First, make sure your project is configured to look for libraries in repository `https://maven.atlassian.com/public`.

Next, ensure your scenario can see the gatling-jwt library. For example, if you're using sbt:

    libraryDependencies += "com.atlassian.gatling" %% "gatling-jwt" % "0.1"
    
Or with Maven:

    <dependency>
      <groupId>com.atlassian.gatling</groupId>
      <artifactId>gatling-jwt_2.10</artifactId>
      <version>0.1</version>
    </dependency>

    
### Invoke gatling-jwt from your Scenario
    
Just swap your `http("request-name")` for `httpWithJwt("request-name")`, and call `.sign("consumerKey", "userKey", "sharedSecret")` after adding all your querystring parameters.

For example:

    import scala.concurrent.duration.DurationInt
    
    import com.atlassian.gatling.jwt.Predef.httpWithJwt
    
    import io.gatling.core.Predef.{Simulation, UserNumberImplicit}
    import io.gatling.core.Predef.{checkBuilder2Check, extractorCheckBuilder2MatcherCheckBuilder, ramp, scenario,     stringToExpression, value2Expression}
    import io.gatling.core.Predef.assertions.global
    import io.gatling.http.Predef.{http, httpProtocolBuilder2HttpProtocol, requestBuilder2ActionBuilder, status}
    
    class SimpleJwt extends Simulation {
    
      val httpProtocol = http.baseURL("https://example.com")
    
      val simpleJwtScenario = scenario("Send a JWT signed request")
        .exec(
          httpWithJwt("my-jwt-request")
            .get("/some-resource")
            .queryParam("param_1", "value_1")
            .queryParam("param_2", "value_2")
            .sign("${host}", "${user}", "${secret}")
            .check(status.is(200)))
    
      setUp(simpleJwtScenario.inject(ramp(5 users) over (5 seconds)))
        .protocols(httpProtocol)
        .assertions(global.successfulRequests.percent.is(100))
    
    }
    
    
## Development

Hack away, then `sbt clean publish` to get a snapshot in your local repo.

To release a new version: `sbt release`.