package com.atlassian.gatling.jwt

import io.gatling.core.session.Expression

object Predef {

  def httpWithJwt(requestName: Expression[String]) = JwtRequestBaseBuilder.jwt(requestName)

}