package com.atlassian.gatling.jwt

import com.atlassian.jwt.core.writer.JsonSmartJwtJsonBuilder
import com.atlassian.jwt.core.TimeUtil
import com.atlassian.jwt.httpclient.CanonicalHttpUriRequest
import collection.JavaConversions._
import com.atlassian.jwt.core.writer.JwtClaimsBuilder
import com.atlassian.jwt.core.writer.NimbusJwtWriterFactory
import com.atlassian.jwt.SigningAlgorithm
import java.util.concurrent.TimeUnit

/**
 * Simple client to atlassian-jwt library for generating a signature from the relevent attributes.
 */
object JwtSigner {

  val JWT_EXPIRY_WINDOW_SECONDS_DEFAULT = TimeUnit.MINUTES.toSeconds(3)

  def generateSignature(httpMethod: String,
    path: String,
    parameters: Map[String, Array[String]],
    consumerKey: String,
    userKey: String,
    sharedSecret: String) = {

    val jsonBuilder = new JsonSmartJwtJsonBuilder()
      .issuedAt(TimeUtil.currentTimeSeconds())
      .expirationTime(TimeUtil.currentTimePlusNSeconds(JWT_EXPIRY_WINDOW_SECONDS_DEFAULT))
      .issuer(consumerKey)
      .subject(userKey)

    val request = new CanonicalHttpUriRequest(httpMethod, path, "", parameters)

    JwtClaimsBuilder.appendHttpRequestClaims(jsonBuilder, request)

    new NimbusJwtWriterFactory()
      .macSigningWriter(SigningAlgorithm.HS256, sharedSecret)
      .jsonToJwt(jsonBuilder.build())
  }
}