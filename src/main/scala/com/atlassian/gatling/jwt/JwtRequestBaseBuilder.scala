package com.atlassian.gatling.jwt

import io.gatling.core.session.Expression
import io.gatling.http.request.builder.GetHttpRequestBuilder
import io.gatling.http.request.builder.HttpAttributes
import io.gatling.http.request.builder.HttpParam
import io.gatling.core.session.Session
import io.gatling.core.Predef._
import io.gatling.http.request.builder.AbstractHttpRequestBuilder

object JwtRequestBaseBuilder {
  def jwt(requestName: Expression[String]) = new JwtRequestBaseBuilder(requestName)
}

class JwtRequestBaseBuilder(requestName: Expression[String]) {

  def get(url: Expression[String]) = GetJwtRequestBuilder(requestName, url)

  // TODO: POST, PUT, DELETE, OPTIONS
}

object GetJwtRequestBuilder {

  def apply(requestName: Expression[String], url: Expression[String]) = new GetJwtRequestBuilder(HttpAttributes(requestName, "GET", url))

}

class GetJwtRequestBuilder(httpAttributes: HttpAttributes) extends AbstractHttpRequestBuilder[GetJwtRequestBuilder](httpAttributes) {

  override def newInstance(httpAttributes: HttpAttributes) = new GetJwtRequestBuilder(httpAttributes)

  /**
   * Calculates JWT signature based on method, URL and params added so far, then adds the signature as extra query string parameter 'jwt'
   * Todo: some of the params should be options.
   */
  def sign(consumerKey: Expression[String], userKey: Expression[String], sharedSecret: Expression[String]): GetJwtRequestBuilder = {

    val signatureParamValue: Expression[Seq[String]] = { session: Session =>

      // Need to convert from httpAttributes.queryParams to params argument to JwtSigner.generate().
      // So that's from  List[(Expression[String], Expression[Seq[String]])] to Map[String, Array[String]]
      // HACK: Couldn't figure out how to do that without mutation.
      val paramsMutableMapHack = collection.mutable.Map[String, Array[String]]()
      httpAttributes.queryParams.foreach({ param =>
        for {
          paramName <- param._1(session)
          paramValues <- param._2(session)
        } yield (paramsMutableMapHack.put(paramName, paramValues.toArray))
      })

      for {
        url <- httpAttributes.url(session)
        cKey <- consumerKey(session)
        uKey <- userKey(session)
        sSecret <- sharedSecret(session)
      } yield Seq(JwtSigner.generateSignature(httpAttributes.method, url, paramsMutableMapHack.toMap, cKey, uKey, sSecret))

    }

    val signatureParam: HttpParam = ("jwt", signatureParamValue)
    newInstance(httpAttributes.copy(queryParams = signatureParam :: httpAttributes.queryParams))

  }
}

